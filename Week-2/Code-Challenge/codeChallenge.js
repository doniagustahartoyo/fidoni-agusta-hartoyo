const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < i; j++) {
      if (data[i] < data[j]) {
        let temp = data[i];
        data[i] = data[j];
        data[j] = temp;
      }
    }
  }
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortDecending(data) {
  // Code Here
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < i; j++) {
      if (data[i] > data[j]) {
        let temp = data[i];
        data[i] = data[j];
        data[j] = temp;
      }
    }
  }
  return data.filter((i) => typeof i === "number");
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
