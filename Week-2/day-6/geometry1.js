const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function cube(side) {
  return side * side * 6;
}

function inputSide(side) {
  rl.question(`Side: `, (side) => {
    if (!isNaN(side)) {
      console.log(`\nCube: ${cube(side)}`);
      rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputSide(side);
    }
  });
}

console.log(`Cube`);
console.log(`=========`);
inputSide();
