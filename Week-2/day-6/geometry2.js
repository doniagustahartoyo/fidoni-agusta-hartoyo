const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function tube(r, t) {
  console.log("return: " + 2 * 3.14 * r * (r + t));
  return 2 * 3.14 * r * (r + t);
}

function input() {
  rl.question("Rusuk: ", function (r) {
    rl.question("Tinggi: ", (t) => {
      if (r > 0 && t > 0) {
        console.log(`\nTube: ${tube(r, t)}`);
        rl.close();
      } else {
        console.log(`r and t must be a number\n`);
        input();
      }
    });
  });
}

console.log("Tube");
console.log("===========");
input();
