const EventEmitter = require("events");

const readline = require("readline");

const my = new EventEmitter();

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

my.on("Login:failed", function (email) {
  console.log(`${email} is failed to login for access case!`);
  rl.close();
});

my.on("Login:sucess", function (email) {
  console.log(`${email} is success to login for access case!`);

  rl.close();
});

function login(email, password) {
  const passwordInDatabase = "qwerty123";
  if (password !== passwordInDatabase) {
    my.emit("Login:failed", email);
  } else {
    my.emit("Login:success", email);
    const fileCase = require("../day-9/assignmentNumber2");
    rl.close();
  }
}

rl.question("Email: ", (email) => {
  rl.question("Password: ", (password) => {
    login(email, password);
  });
});
