const router = require("express").Router();

const { isSignedIn } = require("../middlewares/auth");

const { getAllTask, getDetailTask, createTask, updateTask, deleteTask } = require("../controllers/task");

router.get("/", isSignedIn, getAllTask);

router.get("/", isSignedIn, createTask);

module.exports = router;
