const router = require("express").Router();

const { signIn } = require("../controllers/signIn");
const { signUp } = require("../controllers/user");

router.post("/signUp", signUp);
router.post("/signIn", signIn);

module.exports = router;
