const { User } = require("../models");

const { encryptPass } = require("../helpers/encryption");

class UsersController {
  static async signUp(req, res, next) {
    let statusCode;
    const firstName = req.body.firsName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = encryptPass(req.body.password);

    const data = { firstName, lastName, email, password };

    User.create(data)
      .then((user) => {
        if (user) {
          statusCode = 201;
          delete user.dataValues.password;
          let output = {
            statusCode,
            userCreated: user,
          };

          res.status(201).json(output);
        }
      })
      .catch((err) => {
        console.log("err", err);
        next(err);
      });
  }
}

module.exports = new UsersController();
