const { task } = require("../models");

class Task {
  static async getAllTask(req, res, next) {
    try {
      let data = await task.findAll({
        where: { id_user: req.userData.id },
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Task not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  static async getDetailTask(req, res, next) {
    try {
      let data = await task.findOne({
        where: { id: req.params.id },
      });

      if (!data) {
        return res.status(404).json({ errors: ["Task not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  static async createTask(req, res, next) {
    try {
      const createData = await task.create(req.body);

      const data = await task.findOne({
        where: {
          id: createData.id,
        },
      });

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  static async updateTask(req, res, next) {
    try {
      const updateData = await task.update(req.body, {
        where: {
          id: req.params.id,
        },
      });

      if (updateData[0] === 0) {
        return res.status(404).json({ errors: ["Task not found!"] });
      }
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  static async deleteTask(req, res, next) {
    try {
      let data = await task.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({ errors: ["Task not found!"] });
      }
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Task();
