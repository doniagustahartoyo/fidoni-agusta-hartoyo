const validator = require("validator");

exports.createOrUpdateTaskValidator = async (req, res, next) => {
  try {
    const errors = [];

    if ((validator.isEmpty(req.body.task), { ignore_whitespace: false })) {
      errors.push("Please input the task");
    }

    next();
  } catch (error) {
    next(error);
  }
};
