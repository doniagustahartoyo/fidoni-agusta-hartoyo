const express = require("express");

const user = require("./routes/user");
const task = require("./routes/tasks");

const errorHandler = require("./middlewares/errorHandler");

const port = process.env.PORT || 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", user);
app.use("/tasks", task);

app.all("*", (req, res, next) => {
  next({ statusCode: 404, message: "Endpoint not found" });
});

app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server running on ${port}!`);
});
