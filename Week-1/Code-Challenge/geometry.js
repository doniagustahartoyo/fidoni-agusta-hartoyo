// Cube Return
function cubeReturn(side) {
  console.log("return: " + side * side * 6);
  return side * side * 6;
}

// Cube no Return
function cubeNoReturn(side) {
  console.log("no return: " + side * side * 6);
}

let cubeOne = cubeReturn(36);
let cubeTwo = cubeReturn(56);
console.log("one + two: " + (cubeOne + cubeTwo));
console.log("one - two: " + (cubeOne - cubeTwo));

let cubeThree = cubeNoReturn(8);
let cubeFour = cubeNoReturn(4);
console.log("three + four: " + (cubeThree + cubeFour));
console.log("three - four: " + (cubeThree - cubeFour));

// Tube
function tubeReturn(r, t) {
  console.log("return: " + 2 * 3.14 * r * (r + t));
  return 2 * 3.14 * r * (r + t);
}

let tubeOne = tubeReturn(14, 10);
let tubeTwo = tubeReturn(14, 16);
console.log("one + two: " + (tubeOne + tubeTwo));

function tubeNoReturn(r, t) {
  console.log("no return: " + 2 * 3.14 * r * (r + t));
}

let tubeThree = tubeNoReturn(26, 31);
let tubeFour = tubeNoReturn(58, 21);
console.log("three - four: " + (tubeThree - tubeFour));

// cube + tube
console.log("cube1 + tube1: " + (cubeOne + tubeOne));
console.log("cube2 - tube2: " + (cubeTwo - tubeTwo));
