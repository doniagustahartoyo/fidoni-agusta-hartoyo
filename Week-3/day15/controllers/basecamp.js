let data = require("../models/data.json");

class Basecamp {
  getAllBasecamp(req, res, next) {
    try {
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  creatDataBc(req, res, next) {
    try {
      let lastId = data[data.length - 1].id;

      data.push({
        id: lastId + 1,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
      });

      res.status(201).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  updateDataBc(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      if (!findData) {
        return res.status(404).json({ errors: ["Data Basecamp not found"] });
      }

      data = data.map((item) =>
        item.id === parseInt(req.params.id)
          ? {
              id: parseInt(req.params.id),
              username: req.body.username,
              email: req.body.email,
              password: req.body.password,
              job: req.body.job,
            }
          : item
      );
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  deleteDataBc(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.lastId));

      if (!findData) {
        return res.status(404).json({ errors: ["Data Basecamp not found"] });
      }
      data = data.filter((item) => item.id !== parseInt(req.params.id));

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }
}

module.exports = new Basecamp();
