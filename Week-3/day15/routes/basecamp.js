const express = require("express");

const { getAllBasecamp, creatDataBc, updateDataBc, deleteDataBc } = require("../controllers/basecamp.js");

const router = express.Router();

router.get("/", getAllBasecamp);

router.post("/", creatDataBc);

router.put("/:id", updateDataBc);

router.delete("/:id", deleteDataBc);

module.exports = router;
