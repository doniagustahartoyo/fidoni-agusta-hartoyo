const express = require("express");
const basecamp = require("./routes/basecamp");
const app = express();

const port = process.env.PORT || 3001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/basecamp", basecamp);

app.listen(port, () => {
  console.log(`Server running on ${port}!`);
});
