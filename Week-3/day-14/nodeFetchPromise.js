const fetch = require("node-fetch");

let urlUsers = "https://61715d5fc20f3a001705fc65.mockapi.io/users";
let urlPosts = "https://61715d5fc20f3a001705fc65.mockapi.io/post";
let urlAlbums = "https://61715d5fc20f3a001705fc65.mockapi.io/albums";
/* 
function getUsers() {
  fetch(urlUsers)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.error(err.message);
    });
}
getUsers();

function getPosts() {
  fetch(urlPosts)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.error(err.message);
    });
}
getPosts();

function getAlbums() {
  fetch(urlAlbums)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err.message);
    });
}
getAlbums();
*/

fetch(urlUsers)
  .then((response) => {
    console.log("resolved :", response);
    return response.json();
  })
  .then((data) => {
    console.log(data);
    return fetch(urlPosts);
  })
  .then((response) => {
    console.log("resolved fetch ke-2 :", response);
    return response.json();
  })
  .then((data) => {
    console.log(data);
    return fetch(urlAlbums);
  })
  .then((response) => {
    console.log("resolved fetch ke-3 :", response);
    return response.json();
  })
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error(error.message);
  });
