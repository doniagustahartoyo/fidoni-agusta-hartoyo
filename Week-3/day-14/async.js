const axios = require("axios");

let urlPosts = "https://61715d5fc20f3a001705fc65.mockapi.io/post";
let urlUsers = "https://61715d5fc20f3a001705fc65.mockapi.io/users";
let urlAlbums = "https://61715d5fc20f3a001705fc65.mockapi.io/albums";
let data = {};

const fetchApi = async () => {
  try {
    let response = await Promise.all([axios.get(urlPosts), axios.get(urlUsers), axios.get(urlAlbums)]);

    data = {
      posts: response[0].data.map((item) => {
        return { title: item.title, userId: item.userId };
      }),
      users: response[1].data,
      albums: response[2].data,
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
