const fetch = require("node-fetch");

const getAllData = async () => {
  let urlPosts = "https://61715d5fc20f3a001705fc65.mockapi.io/post";
  let urlUsers = "https://61715d5fc20f3a001705fc65.mockapi.io/users";
  let urlAlbums = "https://61715d5fc20f3a001705fc65.mockapi.io/albums";
  let data = {};

  try {
    const jsonPosts = await fetch(urlPosts);
    const jsonUsers = await fetch(urlUsers);
    const jsonAlbums = await fetch(urlAlbums);
    let response = await Promise.all([jsonPosts.json(), jsonUsers.json(), jsonAlbums.json()]);

    data = {
      posts: response[0],
      users: response[1],
      albums: response[2],
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

getAllData();
