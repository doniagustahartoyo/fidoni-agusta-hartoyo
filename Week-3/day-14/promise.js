const axios = require("axios");

let urlPosts = "https://61715d5fc20f3a001705fc65.mockapi.io/post";
let urlUsers = "https://61715d5fc20f3a001705fc65.mockapi.io/users";
let urlAlbums = "https://61715d5fc20f3a001705fc65.mockapi.io/albums";
let data = {};

axios
  .get(urlPosts)
  .then((response) => {
    data = {
      posts: response.data.map((item) => {
        return { title: item.title, userId: item.userId };
      }),
    };

    return axios.get(urlUsers);
  })
  .then((response) => {
    data = { ...data, users: response.data };

    return axios.get(urlAlbums);
  })
  .then((response) => {
    data = { ...data, albums: response.data };
    console.log(data);
  })
  .catch((err) => {
    console.error(err.message);
  });
